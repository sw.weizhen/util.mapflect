package mapflect

import (
	"errors"
	"fmt"
	"sort"
	"strings"
)

func (ref *Error) Error() string {
	pts := make([]string, len(ref.Errors))
	for i, err := range ref.Errors {
		pts[i] = fmt.Sprintf("* %s", err)
	}

	sort.Strings(pts)
	return fmt.Sprintf("%d error marshalling:\n\n%s", len(ref.Errors), strings.Join(pts, "\n"))
}

func (ref *Error) WrappedErrors() []error {
	if ref == nil {
		return nil
	}

	result := make([]error, len(ref.Errors))
	for i, e := range ref.Errors {
		result[i] = errors.New(e)
	}

	return result
}

func appendErrors(errors []string, err error) []string {
	switch e := err.(type) {
	case *Error:
		return append(errors, e.Errors...)
	default:
		return append(errors, e.Error())
	}
}
