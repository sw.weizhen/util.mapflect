package main

import (
	"fmt"

	mrf "gitlab.com/sw.weizhen/util.mapflect"
)

type Person struct {
	NaXXme              string `mapflect:"name"`
	Age                 int
	Pass                bool
	Address             []interface{}
	NamMap              map[string]int
	StrMap              map[string]string
	Dettt1wefwefcwefwef Detail `mapflect:"det"` // Nest struct
	Slice               []int
}

type Detail struct {
	Balance int
	Date    string
}

type MapS struct {
	Str1 string  `mapflect:"Str1"`
	Str2 string  `mapflect:"Str2"`
	C    float32 `mapflect:"Float1"`
	D    int     `mapflect:"Int1"`
}

var mpPerson map[string]interface{}
var mpS map[string]string

func init() {
	mpPerson = map[string]interface{}{
		"pass":    true,
		"name":    "S.W.",
		"age":     999,
		"address": []interface{}{"404", "405", "406"},
		"nammap":  map[string]int{"M1": 0, "M2": 1, "M3": 2, "M4": 3},
		"strmap":  map[string]string{"S1": "0", "S2": "0", "S3": "0", "S4": "0", "S5": "0"},
		"det":     map[string]interface{}{"balance": 999, "date": "none"},
		"slice":   []int{1, 3, 5, 7},
	}

	mpS = map[string]string{
		"Str1":   "a",
		"Str2":   "b",
		"Float1": "2.15",
		"Int1":   "2",
	}
}

func main() {
	fmt.Println("zygote::main()")
	p1 := Person{}

	// fmt.Printf("%+v\n", p1)
	mrf.Marshal(mpPerson, &p1)
	fmt.Printf("%+v\n", p1)

	p2 := MapS{}
	mrf.Marshal(mpS, &p2)
	fmt.Println(p2.C + 2) // out put: 4.15
}
