package mapflect

const (
	EMPTY = ""
	NIL   = "0"
	ONE   = "1"
)

const (
	SQUARE_BRACKETS_L = "["
	SQUARE_BRACKETS_R = "]"
	PERIOD            = "."
	COMMA             = ","
	DASH              = "-"
)

const (
	TAG = "mapflect"
	// OMITEMPTY = "omitempty"
	// SQUASH    = "squash"
	// REMAIN    = "remain"
)
